
Hotkey
------------------------
By: Shawn Conn (http://www.shawnconn.com/contact)

This module allows you to place hotkeys (HTML attribute accesskey) onto Drupal 
forms. After becoming accustomed to editing much on the Wikipedia, I found it 
really useful for editing many pages to use the accesskey attribute placed on 
MediaWiki's edit buttons. Likewise, it can become tedious when editing many 
Drupal nodes to have to scroll to the bottom of the form to click the submit 
button.

This module remedies this by allowing you to automatically insert an accesskey 
attribute on any drupal form button or drupal node page. Be aware that because 
of the lack of a standard implementation for accesskeys across web browsers you 
might end up with a shortcut key conflict with your browser.

To install, place the hotkey folder into your modules directory.
Go to administer -> site building -> modules to enable the module.
Set the permissions at administer -> user management
Upon installation, the module will add a few default hotkeys any other hotkey 
can be added by going to administer -> site configuration -> Hotkey settings
